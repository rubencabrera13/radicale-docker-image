FROM python:3.6.8-stretch

RUN apt update && apt upgrade -y && apt install -y uwsgi uwsgi-plugin-python3

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt
RUN rm /requirements.txt && mkdir /var/radicale && mkdir /etc/radicale

VOLUME ["/uwsgi.ini", "/etc/radicale/config", "/etc/radicale/passwd", "/var/radicale"]

EXPOSE 5232

CMD ["uwsgi", "--ini", "/uwsgi.ini"]
