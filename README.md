### Docker image for radicale cardDAV/calDAV server

Official docs https://radicale.org/configuration/

Build for own use.

Container with uwsgi server and radicale version 2.1.11.

All config files mounted as external volumes. As the storage folders. Check Dockerfile.

Built image: `nekatak2017/radicale:2.1.11`.
